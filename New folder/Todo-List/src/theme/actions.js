import { SET_THEME } from './constant';

export function setTheme(theme) {
  return {
    type: SET_THEME,
    theme,
  };
}
