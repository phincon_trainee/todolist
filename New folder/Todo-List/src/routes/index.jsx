import MainLayout from '@layouts/MainLayout';

// import Home from '@pages/Home';
import NotFound from '@pages/NotFound';
import Content from '@components/Content';

const routes = [
  {
    path: '/',
    name: 'Content',
    component: Content,
  },

  { path: '*', name: 'Not Found', component: NotFound, layout: MainLayout },
];

export default routes;
