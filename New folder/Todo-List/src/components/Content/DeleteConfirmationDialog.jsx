/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React from 'react';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button';

const DeleteConfirmationDialog = ({ open, onClose, onDelete }) => {
  const handleDelete = () => {
    onDelete(true); // Pass true to indicate confirmed deletion
    onClose(); // Close the dialog
  };

  const handleCancel = () => {
    onDelete(false); // Pass false to indicate cancellation
    onClose(); // Close the dialog
  };

  return (
    <Dialog open={open} onClose={handleCancel}>
      <DialogTitle>Delete Confirmation</DialogTitle>
      <DialogContent>
        <DialogContentText>Are you sure you want to delete this item?</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCancel} color="primary">
          Cancel
        </Button>
        <Button onClick={handleDelete} color="secondary">
          Delete
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DeleteConfirmationDialog;
