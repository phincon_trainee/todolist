/* eslint-disable no-unused-vars */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable react/no-unknown-property */
/* eslint-disable no-shadow */
import React, { useState, useEffect } from 'react';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useDispatch, connect } from 'react-redux';
import { IconButton, Paper, InputBase } from '@mui/material';
import uuid from 'react-uuid';
import WbSunnyIcon from '@mui/icons-material/WbSunny';
import DarkModeIcon from '@mui/icons-material/DarkMode';
import EditIcon from '@mui/icons-material/Edit';
import CloseIcon from '@mui/icons-material/Close';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import Tooltip from '@mui/material/Tooltip';
import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';
import RadioButtonCheckedIcon from '@mui/icons-material/RadioButtonChecked';

import PropTypes from 'prop-types';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';

import { createStructuredSelector } from 'reselect';
import classes from '@components/Content/style.module.scss';
import DeleteConfirmationDialog from './DeleteConfirmationDialog';
import { selectTodo, selectTodoFilter, selectTodoActive, selectTodoEdit } from '../../ToDo/selector';
import {
  setTodo,
  setTodoClear,
  setTodoFilter,
  setTodoActive,
  setTodoRemove,
  setTodoEdit,
  reorderTodo,
} from '../../ToDo/actions';
import { selectTheme } from '../../theme/selector';
import { setTheme } from '../../theme/actions';

const Content = ({ theme, todo, todoFilter }) => {
  const dispatch = useDispatch();
  const [editItemId, setEditItemId] = useState(null);

  const [activeFilter, setActiveFilter] = useState('all');

  const isLight = theme === 'dark';

  const handleTheme = () => {
    if (theme === 'light') {
      dispatch(setTheme('dark'));
    } else {
      dispatch(setTheme('light'));
    }
  };

  const handleKeyDown = (event) => {
    if (event.key === 'Enter') {
      event.preventDefault();
      const ObjTodo = {
        id: uuid(),
        data: event.target.value,
        status: 'active',
      };

      dispatch(setTodo(ObjTodo));
      event.target.value = '';

      localStorage.setItem('todo', JSON.stringify([...todo, ObjTodo]));
    }
  };

  const handleFilter = (selectedFilter) => {
    setActiveFilter(selectedFilter);

    dispatch(setTodoFilter(selectedFilter));
  };

  const handleActive = (id) => {
    const toggleActive = todo.map((todo) =>
      todo.id === id
        ? {
            ...todo,
            status: todo.status === 'active' ? 'completed' : 'active',
          }
        : todo
    );

    dispatch(setTodoActive(toggleActive));
  };

  const handleEditChange = (id, value) => {
    const editedTodo = todo.map((todoItem) => (todoItem.id === id ? { ...todoItem, data: value } : todoItem));

    dispatch(setTodoEdit(editedTodo));
  };

  const handleRemoveTodo = (id) => {
    const remove = todo.filter((todo) => todo.id !== id);

    dispatch(setTodoRemove(remove));
  };

  const handleClearCompleted = (clearcompleted) => {
    const clear = todo.filter((todo) => todo.status !== clearcompleted);

    dispatch(setTodoClear(clear));
  };

  const handleDragEnd = (result) => {
    if (!result.destination) return; // Drop outside the list
    dispatch(reorderTodo(result.source.index, result.destination.index));
  };

  useEffect(() => {
    // Save to local storage whenever todo state changes
    localStorage.setItem('todo', JSON.stringify(todo));
  }, [todo]);

  let filteredTodos;
  if (todoFilter === 'active') {
    filteredTodos = todo.filter((item) => item.status === 'active');
  } else if (todoFilter === 'completed') {
    filteredTodos = todo.filter((item) => item.status === 'completed');
  } else {
    filteredTodos = todo;
  }

  const completedCount = filteredTodos ? filteredTodos.reduce((count) => (count += 1), 0) : 0;

  const [showDeleteDialog, setShowDeleteDialog] = useState(false);
  const [todoToDelete, setTodoToDelete] = useState(null);

  const handleOpenDeleteDialog = (id) => {
    setTodoToDelete(id);
    setShowDeleteDialog(true);
  };

  // Function to close the delete confirmation dialog
  const handleCloseDeleteDialog = () => {
    setShowDeleteDialog(false);
    setTodoToDelete(null);
  };

  // Function to delete the todo item after confirmation
  const handleDeleteTodo = (id) => {
    // If the user confirms the deletion, delete the todo item
    handleRemoveTodo(id);

    // Close the delete confirmation dialog
    handleCloseDeleteDialog();
  };
  const handleDeleteConfirmation = (confirmed) => {
    if (confirmed) {
      // If the user confirms the deletion, delete the todo item
      handleRemoveTodo(todoToDelete);
    }

    // In any case, close the delete confirmation dialog
    handleCloseDeleteDialog();
  };

  const handleEdit = (id) => {
    if (editItemId === id) {
      setEditItemId(null);
    } else {
      setEditItemId(id);
    }

    // Close the delete confirmation dialog if it is open
    handleCloseDeleteDialog();
  };

  const myTheme = createTheme({
    palette: {
      background: {
        default: isLight ? 'hsl(207, 26%, 17%)' : 'hsl(0, 0%, 98%)',
        paper: isLight ? 'hsl(237, 14%, 26%)' : 'hsl(0, 0%, 98%)',
      },
      text: {
        primary: isLight ? 'hsl(241, 19%, 90%)' : 'hsl(200, 15%, 8%)',
      },
    },
  });

  return (
    <ThemeProvider theme={myTheme}>
      <div className={isLight ? classes.containerDark : classes.container}>
        <div className={isLight ? classes.pageUpDark : classes.pageUp}>
          <div className={classes.content}>
            <div className={classes.header}>
              <div className={classes.leftHeader}>
                <h1>TODO</h1>
              </div>
              <div className={classes.rightHeader} variant="contained" onClick={handleTheme}>
                <IconButton>
                  {theme === 'light' ? (
                    <DarkModeIcon sx={{ fontSize: 36, color: 'white' }} />
                  ) : (
                    <WbSunnyIcon sx={{ fontSize: 36, color: 'white' }} />
                  )}
                </IconButton>
              </div>
            </div>
            <div className={classes.input}>
              <Paper component="form" sx={{ p: '0.6rem 1rem', display: 'flex', alignItems: 'center' }}>
                <IconButton>
                  <RadioButtonUncheckedIcon />
                </IconButton>
                <InputBase
                  sx={{
                    ml: 1,
                    flex: 1,
                    fontFamily: `'Josefin Sans', sans-serif`,
                    fontSize: '16px',
                    padding: '1px',
                    marginTop: '4px',
                  }}
                  placeholder="Currently Typing"
                  onKeyDown={handleKeyDown}
                />
              </Paper>
            </div>
            <br />
            <div className={classes.display}>
              {filteredTodos?.length > 0 ? (
                <Paper sx={{ p: '0.6rem 1rem', alignItems: 'center' }} style={{ maxHeight: 350, overflow: 'auto' }}>
                  {filteredTodos.map((item) => (
                    <div className={isLight ? classes.displayText : classes.displayTextDark} key={item.id}>
                      <div className={classes.itemContainer}>
                        {editItemId === item.id ? (
                          <InputBase
                            value={item.data}
                            onChange={(e) => handleEditChange(item.id, e.target.value)}
                            autoFocus
                          />
                        ) : (
                          <>
                            <Tooltip title={item.status === 'active' ? 'Completed' : 'Active'}>
                              <IconButton onClick={() => handleActive(item.id)}>
                                {item.status === 'completed' ? (
                                  <RadioButtonCheckedIcon style={{ color: 'blue' }} />
                                ) : (
                                  <RadioButtonUncheckedIcon />
                                )}
                              </IconButton>
                            </Tooltip>
                            <div
                              className={item.status === 'completed' ? classes.valueActive : classes.value}
                              onDoubleClick={() => handleEdit(item.id)}
                            >
                              {item.data}
                            </div>
                          </>
                        )}
                      </div>
                      <div className={classes.edit}>
                        <IconButton sx={{ p: '10px' }} aria-label="menu" onClick={() => handleEdit(item.id)}>
                          {editItemId === item.id ? (
                            <CheckCircleIcon sx={{ color: 'green', fontSize: '22px' }} />
                          ) : (
                            <EditIcon sx={{ color: 'grey', fontSize: '22px' }} />
                          )}
                        </IconButton>
                        <IconButton
                          sx={{ p: '10px' }}
                          aria-label="menu"
                          onClick={() => {
                            handleDeleteTodo(item.id);
                          }}
                        >
                          <CloseIcon sx={{ color: 'grey', fontSize: '22px' }} />
                        </IconButton>
                      </div>
                    </div>
                  ))}
                </Paper>
              ) : (
                <div className={isLight ? classes.noData : classes.noDataDark}>No Data Available</div>
              )}
            </div>

            <div className={classes.footerContent}>
              <Paper
                sx={{
                  p: '10px',
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  fontSize: '15px',
                }}
              >
                <div className={classes.footerLeft}>{completedCount} Item left</div>
                <div className={classes.footerMiddle}>
                  <div
                    className={`${classes.All} ${activeFilter === 'all' ? classes.clickedItem : ''}`}
                    onClick={() => handleFilter('all')}
                    // style={{ color: 'blue' }}
                  >
                    All
                  </div>
                  <div
                    className={`${classes.Active} ${activeFilter === 'active' ? classes.clickedActive : ''}`}
                    onClick={() => handleFilter('active')}
                  >
                    Active
                  </div>
                  <div
                    className={`${classes.Completed}  ${activeFilter === 'completed' ? classes.clickedCompleted : ''}`}
                    onClick={() => handleFilter('completed')}
                  >
                    Completed
                  </div>
                </div>
                <div className={classes.clearCompleted} onClick={() => handleClearCompleted('completed')}>
                  Clear Completed
                </div>
              </Paper>
            </div>

            <div className={classes.footerContentMobile}>
              <Paper
                sx={{
                  p: '10px',
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  fontSize: '15px',
                  marginTop: '10px',
                  width: '315px',
                  alignItems: 'center',
                  textAlign: 'center',
                }}
              >
                <div className={classes.mobileFooter}>
                  <div className={classes.footerLeftMobile}>{completedCount} Item left</div>
                  <div className={classes.footerMiddleMobile}>
                    <div
                      className={`${classes.AllMobile}  ${activeFilter === 'all' ? classes.clickedItem : ''}`}
                      onClick={() => handleFilter('all')}
                    >
                      All
                    </div>
                    <div
                      className={`${classes.ActiveMobile}  ${activeFilter === 'active' ? classes.clickedItem : ''}`}
                      onClick={() => handleFilter('active')}
                    >
                      Active
                    </div>
                    <div
                      className={`${classes.CompletedMobile} ${
                        activeFilter === 'completed' ? classes.clickedItem : ''
                      }`}
                      onClick={() => handleFilter('completed')}
                    >
                      Completed
                    </div>
                  </div>
                  <div className={classes.clearCompletedMobile} onClick={() => handleClearCompleted('completed')}>
                    Clear Completed
                  </div>
                </div>
              </Paper>
            </div>

            <div className={classes.footerDragDrop}>
              <h4 className={classes.nameDragDrop}>Drag and drop to reorder list </h4>
            </div>
          </div>
        </div>
      </div>
      <DeleteConfirmationDialog
        open={showDeleteDialog}
        onClose={() => handleCloseDeleteDialog()} // Handle dialog cancel/close
        onDelete={() => handleDeleteTodo(todoToDelete)} // Handle dialog confirmation
      />
    </ThemeProvider>
  );
};

Content.propTypes = {
  theme: PropTypes.string,
  todo: PropTypes.array,
  todoFilter: PropTypes.string,
  todoActive: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  theme: selectTheme,
  todo: selectTodo,
  todoFilter: selectTodoFilter,
  todoActive: selectTodoActive,
  todoEdit: selectTodoEdit,
});

export default connect(mapStateToProps)(Content);
